package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        // check whether sourceFile and targetFile are valid
        if (sourceFile == null || targetFile == null)
            throw new IllegalArgumentException();
        if (!sourceFile.exists())
            return false;

        Map<String, Integer> map = new TreeMap<>();
        BufferedReader bufferedReader = null;
        FileWriter fileWriter = null;
        boolean isReadFile, isWriteFile;

        try {   // creating input and output streams
            bufferedReader = new BufferedReader(new FileReader(sourceFile));
            fileWriter = new FileWriter(targetFile,true);
            isReadFile = readFile(bufferedReader, map);
            isWriteFile = writeFile(fileWriter, map);
        }
        catch (IOException ioe) {
            throw new IllegalArgumentException();
        }
        finally {
            try {
                bufferedReader.close();
                fileWriter.close();
            }
            catch (IOException ioe) {
                return false;
            }
        }
        // if reading from a file and write in file completed successfully return true otherwise false
        return isReadFile && isWriteFile;
    }

    private boolean readFile(BufferedReader bufferedReader, Map<String, Integer> map) {
        String line;
        Integer countLines;
        // reading lines from a file and add it to the map
        try {
            while((line = bufferedReader.readLine()) != null) {
                if(line.isEmpty())
                    continue;
                if(map.containsKey(line)) {
                    countLines = map.get(line);
                    map.put(line, countLines + 1);
                }
                else {
                    map.put(line, 1);
                }
            }
        }
        catch (IOException ioe) {
            return false;
        }

        return true;
    }

    private boolean writeFile(FileWriter fileWriter, Map<String, Integer> map) {
        // add lines to an existing file or create a new file and add it lines
        try {
            for(Map.Entry<String, Integer> keys:map.entrySet()) {
                fileWriter.write(keys.getKey()+"["+keys.getValue()+"]\r\n");
            }
        }
        catch (IOException ioe) {
            return false;
        }

        return true;
    }
}

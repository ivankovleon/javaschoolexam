package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        try {
            Deque<Double> stack = new ArrayDeque<>();
            InfixToPostfix itp = new InfixToPostfix(statement);
            String postfixString = itp.doTrans();               // transform infix string to postfix
            double val;
            double tmpResult = 0;
            double num1, num2;
            if (postfixString.isEmpty())
                return null;
            // split postfix expression, and adding it to the array
            String[] tmp = postfixString.split(" ");

            for (int j = 0; j < tmp.length; j++) {
                if (tmp[j].isEmpty())
                    return null;
                // if the item is not an operator to convert it into a double, and push in the stack
                if (!tmp[j].equals("+") && !tmp[j].equals("-") &&
                        !tmp[j].equals("*") && !tmp[j].equals("/")) {
                    try {
                        val = Double.valueOf(tmp[j]);
                    } catch (NumberFormatException nfe) {
                        return null;
                    }
                    stack.push(val);
                }
                // if the item is an operator to get two operands from the stack and execute the operator
                else {
                    num2 = stack.pop();
                    num1 = stack.pop();

                    if (tmp[j].equals("+")) {
                        tmpResult = num1 + num2;
                    }
                    if (tmp[j].equals("-")) {
                        tmpResult = num1 - num2;
                    }
                    if (tmp[j].equals("*")) {
                        tmpResult = num1 * num2;
                    }
                    if (tmp[j].equals("/")) {
                        if (num2 == 0)                          // check division by zero
                            return null;
                        tmpResult = num1 / num2;
                    }
                    stack.push(tmpResult);                      // push result into the stack
                }
            }

            DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en_US"));
            df.applyPattern("#.####");                          // result output format

            return df.format(stack.pop());
        }
        catch (NullPointerException npe) {
            return null;
        }
    }

    class InfixToPostfix {
        private Deque<Character> infixToPostfixStack;
        private String infixString;
        private String postfixString = "";

        public InfixToPostfix(String in) {
            infixString = in;
            infixToPostfixStack = new ArrayDeque<>();
        }
        // method of converting infix form of expression in postfix
        public String doTrans() {
            // partition expression on operators and operands
            // and push them to the stack in the order given postfix notation
            for (int j = 0; j < infixString.length(); j++) {
                char ch = infixString.charAt(j);
                if(ch == ' ') continue;
                if(ch == '+' || ch == '-') {
                    postfixString += ' ';
                    getOper(ch, 1);
                    continue;
                }
                if(ch == '*' || ch == '/') {
                    postfixString += ' ';
                    getOper(ch, 2);
                    continue;
                }
                if(ch == '(') {
                    infixToPostfixStack.push(ch);
                    continue;
                }
                if(ch == ')') {
                    int res = gotParen(ch);
                    if(res == 0) return "";
                }
                else {
                    postfixString += ch;
                }
            }
            // building a new postfix expressions by pop from the stack
            while (!infixToPostfixStack.isEmpty())
            {
                postfixString += ' ';
                postfixString += infixToPostfixStack.pop();
            }

            return postfixString;
        }
        // this method gives the operations multiply and divide a higher priority than addition and subtraction
        public void getOper(char opThis, int prec1) {
            while (!infixToPostfixStack.isEmpty()) {
                char opTop = infixToPostfixStack.pop();

                if (opTop ==  '(') {
                    infixToPostfixStack.push(opTop);
                    break;
                }
                else {
                    int prec2;
                    if (opTop == '+' || opTop == '-')
                        prec2 = 1;
                    else
                        prec2 = 2;
                    if (prec2 < prec1) {
                        infixToPostfixStack.push(opTop);
                        break;
                    }
                    else {
                        postfixString = postfixString + opTop + ' ';
                    }
                }
            }
            infixToPostfixStack.push(opThis);
        }
        // this method gives in brackets operations a top priority
        public int gotParen(char ch) {
            if(infixToPostfixStack.size() < 2)
                return 0;
            while (!infixToPostfixStack.isEmpty()) {
                char chx = infixToPostfixStack.pop();
                if (chx == '(') {
                    break;
                }
                else {
                    postfixString += ' ';
                    postfixString += chx;
                }
            }

            return 1;
        }
    }
}

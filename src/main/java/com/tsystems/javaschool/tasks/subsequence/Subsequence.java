package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // check whether x and y are valid
        if (x == null || y == null)
            throw new IllegalArgumentException();
        if (x.isEmpty())
            return true;

        int count = x.size();
        boolean test1 = false, test2 = false;
        /*
         * pass on the y comparing with x, if found a match with x,
         * check whether a single element is in y,
         * if so, set the variable test is true, increment j,
         * decrement count, if j is equal to the size of x, cycle break
         */
        for (int i = 0, j = 0; i < y.size(); i++) {
            if (y.get(i) == x.get(j)) {

                if (y.indexOf(x.get(j)) == y.lastIndexOf(x.get(j)))
                    test1 = true;
                else test1 = false;

                j++; count--;
                if (j == x.size())
                    break;
            }
        }
        if (count == 0)
            test2 = true;

        return test1 && test2;
    }
}
